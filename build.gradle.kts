plugins {
    kotlin("jvm") version ("1.9.0")

    id("com.github.weave-mc.weave") version ("8b70bcc707")
}

group = "com.gitlab.candicey.gtbsolver"
version = "0.1.0"

minecraft.version("1.8.9")

repositories {
    mavenCentral()
    maven("https://jitpack.io")
    maven("https://repo.spongepowered.org/maven")
}

dependencies {
    testImplementation(kotlin("test"))

    compileOnly("com.github.Weave-MC:Weave-Loader:v0.2.3")
}

tasks.test {
    useJUnitPlatform()
}

kotlin {
    jvmToolchain(8)
}