# GuessTheBuild Solver

![](assets/ingame.png)

Original mod by [zSkillCode](https://github.com/zSkillCode/gtb-solver)

Download: [Release](https://gitlab.com/candicey-weave/gtb-solver/-/releases)

License: [GNU GPLv3](LICENSE)
