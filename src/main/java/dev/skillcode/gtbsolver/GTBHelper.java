package dev.skillcode.gtbsolver;

import com.gitlab.candicey.gtbsolver.VarKt;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class GTBHelper {

    private static final Pattern pattern = Pattern.compile("(?<=§e).*$");

    private GTBHelper() {
    }

    private static String lastEncryptedWord = "";
    private static Set<String> lastWords = new HashSet<>();

    public static Set<String> possibleWords(final String actionBar) {
        final Optional<String> optionalEncryptedWord = getEncryptedWord(actionBar);
        if (!optionalEncryptedWord.isPresent()) return new HashSet<>();

        final String word = optionalEncryptedWord.get();
        if (word.equals(lastEncryptedWord)) return lastWords;
        lastEncryptedWord = word;

        final Set<String> filteredWords = filterLengthAndWhitespaces(word, VarKt.getWordList());

        if (filteredWords.isEmpty()) return filteredWords;

        final Set<Pair<Integer, Character>> charList = new HashSet<>();
        for (int i = 0; i < word.length(); i++) {
            final char character = word.charAt(i);
            if (character != '_' && character != ' ') charList.add(Pair.of(i, character));
        }

        if (charList.isEmpty()) return filteredWords;

        final Set<String> words = filteredWords.stream().filter(current -> checkWord(current, charList)).collect(Collectors.toSet());

        lastWords = words;
        return words;
    }

    private static Set<String> filterLengthAndWhitespaces(final String encryptedWord, final Set<String> words) {
        final int length = encryptedWord.length();
        final int whitespaces = StringUtils.countMatches(encryptedWord, " ");

        return words.stream().filter(current -> current.length() == length && StringUtils.countMatches(current, " ") == whitespaces).collect(Collectors.toSet());
    }

    private static boolean checkWord(final String word, final Set<Pair<Integer, Character>> charList) {
        final String lowerWord = word.toLowerCase(Locale.ROOT);
        int counter = 0;
        for (final Pair<Integer, Character> pair : charList) {
            if (lowerWord.charAt(pair.getLeft()) == pair.getRight()) {
                counter++;
            }
        }
        return counter == charList.size();
    }

    private static Optional<String> getEncryptedWord(final String actionBar) {
        final Matcher matcher = pattern.matcher(actionBar);
        if (!matcher.find()) return Optional.empty();

        String word = matcher.group().toLowerCase(Locale.ROOT);
        if (word.startsWith(" ")) {
            word = word.substring(1);
        }

        return Optional.of(word);
    }
}
