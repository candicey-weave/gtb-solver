package com.gitlab.candicey.gtbsolver

internal fun isGtbActionBar(actionBar: String): Boolean = actionBar.contains("§b") && actionBar.contains("§e") && actionBar.contains("_")