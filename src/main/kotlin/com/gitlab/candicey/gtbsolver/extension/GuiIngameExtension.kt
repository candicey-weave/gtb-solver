package com.gitlab.candicey.gtbsolver.extension

import net.minecraft.client.gui.GuiIngame

private val recordPlayingField = GuiIngame::class.java.getDeclaredField("recordPlaying").apply { isAccessible = true }
var GuiIngame.recordPlaying: String
    get() = recordPlayingField.get(this) as String
    set(value) {
        recordPlayingField.set(this, value)
    }