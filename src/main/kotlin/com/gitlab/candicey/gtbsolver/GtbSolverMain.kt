package com.gitlab.candicey.gtbsolver

import com.gitlab.candicey.gtbsolver.listener.PlayerTickHook
import net.weavemc.loader.api.Hook
import net.weavemc.loader.api.ModInitializer

class GtbSolverMain : ModInitializer {
    override fun preInit() {
        val hookManagerClass = Class.forName("net.weavemc.loader.HookManager")
        val hookManagerInstance = hookManagerClass.getDeclaredField("INSTANCE").apply { isAccessible = true }.get(null)
        val hooks = hookManagerClass.getDeclaredField("hooks").apply { isAccessible = true }.get(hookManagerInstance) as MutableList<Hook>

        hooks.add(PlayerTickHook)
    }
}