package com.gitlab.candicey.gtbsolver.listener

import com.gitlab.candicey.gtbsolver.extension.recordPlaying
import com.gitlab.candicey.gtbsolver.isGtbActionBar
import com.gitlab.candicey.gtbsolver.mc
import dev.skillcode.gtbsolver.GTBHelper
import jdk.internal.org.objectweb.asm.Type
import net.minecraft.util.ChatComponentText
import net.minecraft.util.EnumChatFormatting.*
import net.weavemc.loader.api.Hook
import net.weavemc.loader.api.util.asm
import org.objectweb.asm.tree.ClassNode

object PlayerTickHook : Hook("net/minecraft/entity/player/EntityPlayer") {
    private var ticks = 0

    private var lastActionBar: String? = null

    private var lastWords: Set<String>? = null

    override fun transform(node: ClassNode, cfg: AssemblerConfig) {
        node.methods
            .find { it.name == "onUpdate" }!!
            .instructions
            .insert(asm {
                invokestatic("com/gitlab/candicey/gtbsolver/listener/PlayerTickHook", "onUpdate", "()V")
            })
    }

    @JvmStatic
    fun onUpdate() {
        if (++ticks != 20) {
            return
        }
        ticks = 0

        val actionBar = mc.ingameGUI.recordPlaying
        if (!isGtbActionBar(actionBar)) {
            return
        }

        if (actionBar == lastActionBar) {
            return
        }
        lastActionBar = actionBar

        val words = GTBHelper.possibleWords(actionBar)
        if (words == lastWords) {
            return
        }
        lastWords = words

        val possibleWords = words.joinToString("$YELLOW, ") { "$AQUA$it" }
        val message = "$GRAY[${LIGHT_PURPLE}GTB-Solver${GRAY}]${YELLOW} Possible words: $possibleWords$YELLOW."
        mc.thePlayer.addChatMessage(ChatComponentText(message))
    }
}