package com.gitlab.candicey.gtbsolver

import net.minecraft.client.Minecraft

internal val mc: Minecraft = Minecraft.getMinecraft()

internal val wordList: Set<String> by lazy {
    val resourcePath = "/gtb-solver/wordlist.txt"
    val resource = GtbSolverMain::class.java.getResourceAsStream(resourcePath) ?: error("Resource $resourcePath not found")
    resource.bufferedReader().readLines().toSet()
}